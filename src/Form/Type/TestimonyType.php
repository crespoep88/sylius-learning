<?php

namespace App\Form\Type;

use Sylius\Bundle\ChannelBundle\Form\Type\ChannelChoiceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;

class TestimonyType extends AbstractResourceType
{
    public function __construct(string $dataClass, array $validationGroups = [])
    {
        parent::__construct($dataClass, $validationGroups);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('author', TextType::class, [
                'label' => 'app.form.testimony.author',
            ])
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => TestimonyTranslationType::class,
                'label' => 'app.form.testimony.translations',
            ])
            ->add('position', IntegerType::class, [
                'label' => 'sylius.ui.position',
            ])
            ->add('stars', IntegerType::class, [
                'label' => 'app.ui.stars'
            ])
            ->add('status', TextType::class, [
                'label' => 'app.ui.status'
            ])
            ->add('createdAt', DateTimeType::class, [
                'label' => 'app.form.testimony.creation_date',
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'sylius.ui.enabled',
            ])
            ->add('channel', ChannelChoiceType::class, [
                'multiple' => false,
                'label' => 'app.form.testimony.channel',
            ])
        ;
    }
}
