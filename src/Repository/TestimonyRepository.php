<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Channel;
use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class TestimonyRepository extends EntityRepository
{
    /**
     * @param Channel $channel
     * @param string|null $locale
     * @return array
     */
    public function findByChannel($channel, ?string $locale = null)
    {
        return $this->createTranslationBasedQueryBuilder($locale)
            ->innerJoin('o.channel', 'channel')
            ->andWhere('channel = :channel')
            ->setParameter('channel', $channel)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string|null $locale
     *
     * @return QueryBuilder
     */
    private function createTranslationBasedQueryBuilder(?string $locale): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('o')
            ->addSelect('translation')
            ->leftJoin('o.translations', 'translation')
        ;

        if (null !== $locale) {
            $queryBuilder
                ->andWhere('translation.locale = :locale')
                ->setParameter('locale', $locale)
            ;
        }

        return $queryBuilder;
    }
}
