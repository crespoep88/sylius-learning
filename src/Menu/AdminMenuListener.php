<?php

namespace App\Menu;

use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminMenuListener
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function addAdminMenuItems(MenuBuilderEvent $event)
    {

        $menu = $menu = $event->getMenu();
        $menuNames = array_keys($menu->getChildren());

        //Add new ones
        $this->addBrandsOfSubMenu($menu);

        //Reorder
        array_unshift($menuNames, 'app');
        $menu->reorderChildren($menuNames);
    }

    /**
     * @param ItemInterface $menu
     */
    private function addBrandsOfSubMenu(ItemInterface $menu): void
    {
        $brandsOf = $menu
            ->addChild('app')
            ->setLabel('App')
        ;

        $brandsOf
            ->addChild('testimony', ['route' => 'app_admin_testimony_index'])
            ->setLabel('Store Testimonies')
            ->setLabelAttribute('icon', 'talk')
        ;

    }
}
